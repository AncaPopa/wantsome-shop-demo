package cart;

import org.junit.Assert;
import org.junit.Test;
import page_object.CartPage;
import page_object.CategoryPage;
import page_object.HomeShopPage;
import page_object.SingleProductPageFactory;
import utils.BaseTestClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CartPage cartPage = homeShopPage.clickOnCart();
        assertEquals("CART", cartPage.getCartTitle());
        assertEquals("Your cart is currently empty.", cartPage.getCartMessage());
    }

    @Test
    public void addToCartAndCheckCartValue() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage categoryPage = homeShopPage.clickOnMenCollection();
        SingleProductPageFactory singleProductPageFactory = categoryPage.clickOnTitleProduct();
        singleProductPageFactory.addToCart();
        assertTrue(singleProductPageFactory.getCartMessage().isDisplayed());
        assertEquals(1, singleProductPageFactory.getCartValue());
        assertTrue(singleProductPageFactory.getProductPrice().equals(singleProductPageFactory.getCartProductPrice()));
        CartPage cartPage = new CartPage(driver);
        Assert.assertEquals(1, cartPage.getCartValue());
    }
}
