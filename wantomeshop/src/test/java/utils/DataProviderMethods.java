package utils;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RunWith(DataProviderRunner.class)
public class DataProviderMethods {

   @DataProvider
    public static Object[][] CSVDataProvider() throws IOException {
        List<String> csvData = readAllLinesFromFile("");
        List<Object[]> outputList = new ArrayList<>();

        for(String line:csvData){
            Object[] user = line.split(",");
            outputList.add(user);
        }

        Object[][] result = new Object[outputList.size()][];
        outputList.toArray(result);
        return result;
    }

    public static List<String> readAllLinesFromFile(String filename) throws IOException {
        List<String> allLines = new ArrayList<>();
        File file = getFileFromResources(filename);

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)){
            String line = br.readLine();
            while(line != null){
                allLines.add(line);
                line = br.readLine();
            }
        }
        return allLines;
    }

    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = DataProviderMethods.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
