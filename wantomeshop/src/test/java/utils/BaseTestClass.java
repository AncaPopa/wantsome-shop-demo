package utils;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BaseTestClass {
    public WebDriver driver;
    private static final String URL = "http://theparceldude.local/";

    @Before
    public void setUpTest() {
        driver = BrowserFactory.getBrowser("chrome");
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get(URL);
    }

   @After
   public void tearDown() {
//        driver.quit();
    }
}