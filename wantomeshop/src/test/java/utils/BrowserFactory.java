package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
    public static WebDriver getBrowser(String browserName) {

        WebDriver driver = null;

        switch (browserName) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver",
                        DriversPath.getDriverDirPath() + "chromedriver" + DriversPath.getDriverExtension());
                //ChromeOptions options = new ChromeOptions();
               // options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
                driver = new ChromeDriver();
                return driver;
            case "firefox":
                System.setProperty("webdriver.gecko.driver",
                        DriversPath.getDriverDirPath() + "geckodriver" + DriversPath.getDriverExtension());
                driver = new FirefoxDriver();
                return driver;
            default:
                throw new IllegalArgumentException("Browser name is not valid!");
        }
    }

}
