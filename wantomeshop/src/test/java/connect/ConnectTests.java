package connect;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import page_object.ConnectPage;
import page_object.HomeShopPage;
import utils.BaseTestClass;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class ConnectTests extends BaseTestClass {

    public static Object[][] CSVDataProvider(String file) throws IOException {
        List<String> csvData = readAllLinesFromFile(file);
        List<Object[]> outputList = new ArrayList<>();
        for (String line : csvData) {
            Object[] user = line.split(",");
            outputList.add(user);
        }

        Object[][] result = new Object[outputList.size()][];
        outputList.toArray(result);
        return result;
    }

    @DataProvider
    public static Object[][] CSVDataProviderFileNoPassword() throws IOException {
        return CSVDataProvider("register_no_password.csv");
    }

    @DataProvider
    public static Object[][] CSVDataProviderFileNoFirstName() throws IOException {
        return CSVDataProvider("register_no_first_name.csv");
    }

    @UseDataProvider("CSVDataProviderFileNoPassword")
    @Test
    public void registerNoPasswordTests(
            String email,
            String password,
            String firstName,
            String lastName,
            String phoneNumber
    ) {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.registerData(email + System.currentTimeMillis(),
                password,
                firstName,
                lastName,
                phoneNumber);
        List<WebElement> passwordError = driver.findElements(By.cssSelector("ul[role='alert']"));
        assertTrue(passwordError.isEmpty());
        connectPage.clickOnRegisterButton();

        assertEquals("https://testare-automata.practica.tech/shop/?page_id=560", driver.getCurrentUrl());
        assertEquals("Error: Please enter an account password.", driver.findElement(By.cssSelector("ul[role='alert']")).getText());

    }

    @UseDataProvider("CSVDataProviderFileNoFirstName")
    @Test
    public void registerNoFirstNameTests(
            String email,
            String password,
            String firstName,
            String lastName,
            String phoneNumber
    ) {

        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.registerData(email + System.currentTimeMillis(),
                password,
                firstName,
                lastName,
                phoneNumber);
        List<WebElement> firstNameError = driver.findElements(By.cssSelector("ul[role='alert']"));
        assertTrue(firstNameError.isEmpty());
        connectPage.clickOnRegisterButton();

        assertEquals("https://testare-automata.practica.tech/shop/?page_id=560", driver.getCurrentUrl());
        assertEquals("Error: First name is required!", driver.findElement(By.cssSelector("ul[role='alert']")).getText());
    }

    @Test
    public void registerWithoutEmail() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.fillInRegisterFields("", "256565898", "user1", "user1.1",
                "02315");
        connectPage.clickOnRegisterButton();
        connectPage.errorMessageOnRegisterWithoutEmail();
        assertEquals("Error: Please provide a valid email address.", connectPage.errorMessageOnRegisterWithoutEmail());
        assertEquals("https://testare-automata.practica.tech/shop/?page_id=560", driver.getCurrentUrl());
    }

    @Test
    public void loginWithoutPassword() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        List<WebElement> alerts = driver.findElements(By.cssSelector("ul[role = 'alert']"));
        //Assert.assertTrue(alerts.size() == 0);
        assertTrue(alerts.isEmpty());
        connectPage.clickOnLogin();
        assertTrue(driver.findElement(By.cssSelector("ul[role = 'alert']")).isDisplayed());
        connectPage.fillInUserNameField("firstName@gmail.com1610019190650");
        connectPage.clickOnLogin();
        assertTrue(driver.findElement(By.xpath("//ul[@class='woocommerce-error']/li")).isDisplayed());
        connectPage.alertMessageOnMissingPassword();
        assertEquals("ERROR: The password field is empty.", connectPage.alertMessageOnMissingPassword());
    }

    @Test
    public void loginWithoutUserName() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        List<WebElement> alerts = driver.findElements(By.cssSelector("ul[role = 'alert']"));
        assertTrue(alerts.isEmpty());
//        connectPage.clickOnLogin();
//        assertTrue(driver.findElement(By.cssSelector("ul[role = 'alert']")).isDisplayed());
//        assertEquals("Error: Username is required.", connectPage.getAlertMessageOnMissingUSerName());
        connectPage.fillInPasswordField("A.123456!");
        connectPage.clickOnLogin();
        assertTrue(driver.findElement(By.cssSelector("ul[role='alert']")).isDisplayed());
        assertEquals("Error: Username is required.", connectPage.getAlertMessageOnMissingUSerName());
    }

    @Test
    public void LoginWithEmptyFields() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.fillInLoginFields("", "");
        List<WebElement> fieldError = driver.findElements(By.cssSelector("ul[role='alert']"));
        assertTrue(fieldError.isEmpty());
        connectPage.clickOnLogin();
        assertEquals("Error: Username is required.", connectPage.getAlertMessageOnMissingUSerName());
    }

    public static List<String> readAllLinesFromFile(String filename) throws IOException {
        List<String> allLines = new ArrayList<>();
        File file = getFileFromResources(filename);

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line = br.readLine();
            while (line != null) { // (line=br.readline())
                allLines.add(line);
                line = br.readLine();
            }
        }

        return allLines;
    }


    /**
     * Return the a File object based on the given file name.
     *
     * @param fileName
     * @return
     */
    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = ConnectTests.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);

        System.out.println(resource);

        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
