package search;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import page_object.HomeShopPage;
import page_object.SearchResultPage;
import utils.BaseTestClass;

import java.util.List;

import static org.junit.Assert.*;

public class SearchTests extends BaseTestClass {
    @Test
    public void checkSearchResults() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        SearchResultPage searchResults = homeShopPage.typeSearch("dress");
        assertTrue(searchResults.checkResultsContainsSearch("dress"));
    }

    @Test
    public void invalidSearch() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        List<WebElement> notFoundMessage = driver.findElements(By.cssSelector(".content-area p"));
        assertTrue(notFoundMessage.isEmpty());
        SearchResultPage searchResultPage = homeShopPage.typeSearch("fiji");
        assertEquals("Sorry, but nothing matched your search terms. Please try again with some different keywords.",
                searchResultPage.getNotFoundMessage());
    }

    @Test
    public void emptySearch() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        SearchResultPage searchResultPage = homeShopPage.typeSearch("");
        assertEquals("https://testare-automata.practica.tech/shop/?s=&submit=Search", driver.getCurrentUrl());
    }
}
