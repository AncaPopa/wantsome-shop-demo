package category;

import org.junit.Assert;
import org.junit.Test;
import page_object.CategoryPage;
import page_object.HomeShopPage;
import utils.BaseTestClass;

public class CategoryTests extends BaseTestClass {

    @Test
    public void checkMenTitle() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage menCollection = homeShopPage.clickOnMenCollection();
        Assert.assertEquals("Men Collection", menCollection.getCategoryTitle());
    }

    @Test
    public void checkWomenTitle() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage womenCollection = homeShopPage.clickOnWomenCollection();
        Assert.assertEquals("Women Collection", womenCollection.getCategoryTitle());
    }
}

