package page_object;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RunWith(DataProviderRunner.class)
public class ConnectPage {
    WebDriver driver;
    public final By PAGE_TITLE = By.className("entry-title");
    public final By LOGIN_BUTTON = By.name("login");
    public final By REGISTER_BUTTON = By.name("register");
    public final By USERNAME_FIELD = By.id("username");
    public final By EMAIL_ADDRESS = By.id("reg_email");
    public final By PASSWORD = By.id("reg_password");
    public final By FIRST_NAME = By.id("registration_field_1");
    public final By LAST_NAME = By.id("registration_field_2");
    public final By PHONE_NUMBER = By.id("registration_field_3");
    public final By EMAIL_ERROR_MESSAGE = By.cssSelector("ul[role='alert']");
    public final By PASSWORD_ERROR_MESSAGE = By.cssSelector("ul[role='alert']");
    public final By LOGIN_PASSWORD = By.id("password");
    public final By REMEMBER_ME_BUTTON = By.name("rememberme");
    public final By SUCCESSFULLY_LOGIN_MESSAGE = By.xpath("//div[@class='woocommerce-MyAccount-content']/p[1]");
    public final By ALERT_MESSAGE_LOGIN_PASSWORD = By.xpath("//ul[@class='woocommerce-error']/li");
    public final By ALERT_MESSAGE_LOGIN_USERNAME = By.cssSelector("ul[role='alert']");


    public ConnectPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageTitle() {
        return driver.findElement(PAGE_TITLE).getText();
    }

    public void fillInRegisterFields(
            String email,
            String password,
            String firstName,
            String lastName,
            String phoneNumber) {

        driver.findElement(EMAIL_ADDRESS).sendKeys(email);
        driver.findElement(PASSWORD).sendKeys(password);
        driver.findElement(FIRST_NAME).sendKeys(firstName);
        driver.findElement(LAST_NAME).sendKeys(lastName);
        driver.findElement(PHONE_NUMBER).sendKeys(phoneNumber);
    }

    public void clickOnRegisterButton() {
        driver.findElement(REGISTER_BUTTON).click();
    }

    public String errorMessageOnRegisterWithoutEmail() {
        return driver.findElement(EMAIL_ERROR_MESSAGE).getText();
    }

    public String errorMessageOnRegisterWithoutPassword() {
        return driver.findElement(PASSWORD_ERROR_MESSAGE).getText();
    }

    public void fillInLoginFields(String user, String password) {
        driver.findElement(USERNAME_FIELD).click();
        driver.findElement(USERNAME_FIELD).sendKeys(user);
        driver.findElement(LOGIN_PASSWORD).sendKeys(password);
    }

    public void clickOnRememberMeButton() {
        driver.findElement(REMEMBER_ME_BUTTON).click();
    }

    public String getLoginMessage() {
        return driver.findElement(SUCCESSFULLY_LOGIN_MESSAGE).getText();
    }

    public void fillInUserNameField(String user) {
        driver.findElement(USERNAME_FIELD).click();
        driver.findElement(USERNAME_FIELD).sendKeys(user);
    }

    public void fillInPasswordField(String password) {
        driver.findElement(LOGIN_PASSWORD).sendKeys(password);
    }


    public void clickOnLogin() {
        driver.findElement(LOGIN_BUTTON).click();
    }

    public String alertMessageOnMissingPassword() {
        return driver.findElement(ALERT_MESSAGE_LOGIN_PASSWORD).getText();
    }

    public String getAlertMessageOnMissingUSerName() {
        return driver.findElement(ALERT_MESSAGE_LOGIN_USERNAME).getText();
    }


    @DataProvider
    public static Object[][] CSVDataProvider() throws IOException {
        List<String> csvData = readAllLinesFromFile("happy_flow_register.csv");
        List<Object[]> outputList = new ArrayList<>();

        for (String line : csvData) {
            Object[] user = line.split(",");
            outputList.add(user);
        }

        Object[][] result = new Object[outputList.size()][];
        outputList.toArray(result);
        return result;
    }

    @UseDataProvider("CSVDataProvider")
    public void registerData(
            String email,
            String password,
            String firstName,
            String lastName,
            String phoneNumber
    ) {

        driver.findElement(EMAIL_ADDRESS).sendKeys(email);
        driver.findElement(PASSWORD).sendKeys(password);
        driver.findElement(FIRST_NAME).sendKeys(firstName);
        driver.findElement(LAST_NAME).sendKeys(lastName);
        driver.findElement(PHONE_NUMBER).sendKeys(phoneNumber);

    }

    public static List<String> readAllLinesFromFile(String filename) throws IOException {
        List<String> allLines = new ArrayList<>();
        File file = getFileFromResources(filename);

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {
            String line = br.readLine();
            while (line != null) {
                allLines.add(line);
                line = br.readLine();
            }
        }
        return allLines;
    }


    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = ConnectPage.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
