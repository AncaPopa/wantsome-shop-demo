package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomeShopPage {
    private WebDriver driver;

    public HomeShopPage(WebDriver driver) {
        this.driver = driver;
    }

    public final By CONNECT_BUTTON = By.id("menu-item-566");
    private final By CART_BUTTON = By.xpath(("//a[text()='Cart']"));
    private final By CATEGORY_BUTTON = By.className("category-toggle");
    private final By MEN_COLLECTION = By.id("menu-item-509");
    private final By WOMEN_COLLECTION = By.id("menu-item-510");
    private final By SEARCH_ICON = By.className("search-icon");
    private final By SEARCH_FIELD = By.className("search-field");


    public ConnectPage clickOnConnect() {
        driver.findElement(CONNECT_BUTTON).click();

        return new ConnectPage(driver);
    }

    public CartPage clickOnCart() {
        driver.findElement(CART_BUTTON).click();
        return new CartPage(driver);
    }

    public void clickOnCategory() {
        driver.findElement(CATEGORY_BUTTON).click();

    }

    public CategoryPage clickOnMenCollection() {
        driver.findElement(MEN_COLLECTION).click();
        return new CategoryPage(driver);
    }

    public CategoryPage clickOnWomenCollection() {
        driver.findElement(WOMEN_COLLECTION).click();
        return new CategoryPage(driver);
    }

    public void clickOnSearchIcon() {
        driver.findElement(SEARCH_ICON).click();
    }

    public SearchResultPage typeSearch(String searchInput) {
        driver.findElement(SEARCH_FIELD).sendKeys(searchInput);
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ENTER);
        return new SearchResultPage(driver);
    }
}
