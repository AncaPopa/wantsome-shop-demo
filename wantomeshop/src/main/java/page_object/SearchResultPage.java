package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultPage {
    WebDriver driver;
    private static By SEARCH_RESULTS = By.cssSelector("[id='primary'] [class='entry-title']");
    private static By NOT_FOUND_MESSAGE = By.cssSelector(".content-area p");

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean checkResultsContainsSearch(String searchInput) {
        List<WebElement> searchResults = driver.findElements(SEARCH_RESULTS);
        for (WebElement result : searchResults) {
            if (!result.getText().toLowerCase().contains(searchInput)) {
                return false;
            }
        }
        return true;
    }

    public String getNotFoundMessage() {
        return driver.findElement(NOT_FOUND_MESSAGE).getText();
    }
}
