package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class CategoryPage {
    WebDriver driver;
    private final By CATEGORY_TITLE = By.className("entry-title");
    private final By CART_ICON = By.className("add_to_cart_button");
    private final By PRODUCT_IMAGE = By.cssSelector(".products-img a img");
    private final By PRODUCT_TITLE = By.xpath("//li[contains(@class ,'post-168')]/h3/a");
    private final By HOVERED_ELEMENTS = By.xpath("//figure//div[@class='products-hover-block']");
    private final By PRODUCT_PRICE = By.xpath("//li[contains(@class,'post-174')]/span/span");
    private final By TOTAL_CART = By.className("cart-total");
    private final By CART_VALUE = By.className("cart-value");

    public CategoryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCategoryTitle() {
        return driver.findElement(CATEGORY_TITLE).getText();
    }

    public SingleProductPageFactory clickOnTitleProduct() {
        driver.findElement(PRODUCT_TITLE).click();
        return new SingleProductPageFactory(driver);
    }

    public void addToCartFromHoveredIcon() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(PRODUCT_IMAGE))
                .build()
                .perform();
        actions.moveToElement(driver.findElement(HOVERED_ELEMENTS))
                .build()
                .perform();
        driver.findElement(CART_ICON).click();

    }

    public String getProductPrice() {
        return driver.findElement(PRODUCT_PRICE).getText();
    }

    public String getTotalCartValue() {
        System.out.println(driver.findElement(By.className("cart-total")).getText());
        return driver.findElement(TOTAL_CART).getText();
    }

    public int getCartValue() {
        return Integer.parseInt(driver.findElement(CART_VALUE).getText());
    }


}

