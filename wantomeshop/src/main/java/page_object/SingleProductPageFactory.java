package page_object;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SingleProductPageFactory {
    WebDriver driver;

    @FindBy(name = "add-to-cart")
    WebElement addToCartButton;

    @FindBy(className = "cart-value")
    @CacheLookup
    WebElement cartValue;

    @FindBy(css = "div[role='alert']")
    WebElement addedProductInCartMessage;

    @FindBy(css = "div[class='summary entry-summary'] p > span")
    WebElement productPrice;

    @FindBy(className = "cart-total")
    WebElement cartProductPrice;

    @FindBy(css = "div[class='woocommerce-notices-wrapper'] div a")
    WebElement viewCartButton;

    public SingleProductPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void addToCart() {
        addToCartButton.click();
    }

    public WebElement getCartMessage() {
        return addedProductInCartMessage;
    }

    public int getCartValue() {
        return Integer.parseInt(cartValue.getText());
    }

    public String getProductPrice() {
        return productPrice.getText();

    }

    public String getCartProductPrice() {
        return cartProductPrice.getText();
    }

    public CartPage clickOnViewCartButton() {
        viewCartButton.click();
        return new CartPage(driver);

    }

    public SingleProductPageFactory addToCart(WebDriver driver) {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage categoryPage = homeShopPage.clickOnMenCollection();
        SingleProductPageFactory singleProductPageFactory = categoryPage.clickOnTitleProduct();
        singleProductPageFactory.addToCart();
        return singleProductPageFactory;
    }
}
