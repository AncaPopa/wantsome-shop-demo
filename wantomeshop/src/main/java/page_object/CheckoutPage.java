package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CheckoutPage {
    private JavascriptExecutor jsExecutor;
    WebDriver driver;
    public static By BILLING_FIRST_NAME = By.id("billing_first_name");
    public static By BILLING_LAST_NAME = By.id("billing_last_name");
    public static By BILLING_STREET_ADDRESS = By.id("billing_address_1");
    public static By BILLING_TOWN = By.id("billing_city");
    public static By BILLING_COUNTY = By.id("billing_state");
    public static By BILLING_POSTCODE = By.id("billing_postcode");
    public static By BILLING_PHONE = By.id("billing_phone");
    public static By BILLING_EMAIL = By.id("billing_email");
    public static By ACCOUNT_PASSWORD = By.id("account_password");
    public static By PLACE_ORDER_BUTTON = By.cssSelector("section[id='search-4'] button");
    public static By SHIP_TO_A_DIFFERENT_ADDRESS_CHECKBOX = By.id("ship-to-different-address-checkbox");
    public static By ORDER_MESSAGE = By.cssSelector("div[class='woocommerce-order'] p:first-child");
    public static By CART_SUBTOTAL = By.cssSelector("tfoot > tr > td >span");

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectCounty(By countyElement, String city) {
        WebElement county = driver.findElement(countyElement);
        Select countyDropdown = new Select(county);
        countyDropdown.selectByVisibleText(city);
    }

    public void fillInBillingDetails(
            String city,
            String firstName,
            String lastName,
            String address,
            String town,
            String zipCode,
            String phone,
            String email,
            String Password
    ) {
        driver.findElement(BILLING_FIRST_NAME).sendKeys(firstName);
        driver.findElement(BILLING_LAST_NAME).sendKeys(lastName);
        driver.findElement(BILLING_STREET_ADDRESS).sendKeys(address);
        driver.findElement(BILLING_TOWN).sendKeys(town);
        driver.findElement(BILLING_POSTCODE).sendKeys(zipCode);
        driver.findElement(BILLING_PHONE).sendKeys(phone);
        driver.findElement(BILLING_EMAIL).sendKeys(email);
        driver.findElement(ACCOUNT_PASSWORD).sendKeys(Password);
        selectCounty(BILLING_COUNTY, city);
    }

    public void clickOnPlaceOrderButton() {
        jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("document.querySelector(\"[id='place_order']\").click()");
    }

    public String getOrderMessage() {
        return driver.findElement(ORDER_MESSAGE).getText();
    }

    public void ShipToDifferentAddressCheckBox() {
        driver.findElement(SHIP_TO_A_DIFFERENT_ADDRESS_CHECKBOX).click();
    }

    public String cartSubtotal() {
        return (driver.findElement(CART_SUBTOTAL).getText());
    }
}
